# Interfaced Model Definition

Interfaced Model Definition adalah sebuah metode menjabarkan _function_ yang akan di-expose dari sebuah Model agar dapat dipakai di model atau bagian lain. Sesuai dengan judulnya, metode penjabaran ini akan menggunakan bantuan fitur **Interface Class** yang ada pada Java.

!!! note
	Cara mendefinisikan Model secara dasar dapat dilihat pada artikel [Basic Model Definition](basic_model_definition).

## Requirement

| Dependency   | Version                |
| ------------ | ---------------------- |
| Core Commons | \>= 3.4012.00-SNAPSHOT |
| Core Web     | _Any_                  |

## Background

Interfaced Model Definition ini diimplementasikan untuk menjawab pertanyaan, "Apakah ada cara yang lebih mudah (Java Code Style prefered) dalam memanggil function yang ada di Model?" . Berikut adalah cara pemanggilan function yang dilakukan pada versi sebelumnya,

```java
/**
* [Function Definition]
* Sebuah function untuk membatalkan sebuah dokumen.
*
* @param uid User ID
* @param ids List of Record ID
* @param context Context
*
* @return Process Result as Boolean
*/
public boolean action_request_cancel(long uid, List<Integer> ids, Map<String, Object> context){
	//... Code Omitted ...
    
    return result;
}

/*
* [Calling Function from Model]
*/
Optional<AbstractModel> optModel = PoolCache.getModel("some.model_name");
if ( optModel.isPresent() ) {
    AbstractModel model = optModel.get();
    
    boolean success = model.invoke("action_request_cancel", 
                        new Class[]{long.class, List.class, Map.class}, // Param Defs
                        Descriptor.SUPERUSER_ID, // uid
                        Ints.asList(testSaleContext.customerOrderId), // ids
                        new HashMap<>()); // context
}
```

Format penulisan pemanggilan function di atas sering membuat salah panggil karena beberapa hal sebagai berikut,

1. _Typo_ ketika penilisan nama function secara literal (menulis sendiri tanpa adanya AutoComplete).
2. Tidak cocoknya penulisan Param Defs (Parameter Definitions) dengan parameter yang diminta oleh function yang dimaksud.
3. Panjangnya perintah yang harus ditulis tanpa adanya _guideline_ (error prone).

## Usage

Terdapat 3 bagian dalam pemakaian metode Interfaced Model Definition, yaitu:

1. Model Interface
2. How To Implements & Super
3. Calls

### Model Interface

Model Interface adalah sebuah _wrapper_ untuk memudahkan mendaftar interface apa saja yang digunakan dalam sebuah model.

```java
import com.efitrac.commons.annotation.ModelInterface;

import java.util.List;
import java.util.Map;

@ModelInterface( "sale.customer_order" ) // (1)
public interface SaleCustomerOrderInterface { // (2)

    interface SubmitAction { // (3)
        boolean submit(long uid, List<Integer> ids, Map<String, Object> context); // (4)
    }
}
```

1. Annotation @ModelInterface digunakan untuk menjelaskan target Model dari interface
2. Format penamaan Interface-Class Model Interface: `{ModelClassName}Interface`. Dari contoh di atas, terdapat ModelClass dengan nama `SaleCustomerOrder`.
3. Format penamaan Inner Interface-Class :  `Sebuah kata benda yang menyatakan aksi`. Format penamaan pada bagian ini masih belum ditentukan secara resmi, dengan demikian dihimbau untuk memberikan nama yang sesuai dan pantas.
4. Format penamaan function: `Sebuah kata kerja`. Format penamaan pada bagian ini masih belum ditentukan secara resmi, dengan demikian dihimbau untuk memberikan nama yang sesuai dan pantas.

!!! note "PERHATIAN!"
	Masing-masing Inner Interface-Class disarankan untuk mempunyai satu function, agar Model Class lain dapat melakukan _override_ terhadap salah satu function saja. Adanya beberapa function dalam satu Inner Interface-Class tidaklah dilarang.

### How To Implements & Super

```java
import static com.efitrac.module.sale.models.SaleCustomerOrderInterface.*; // (1)

@Model(name = "sale.customer_order")
public class SaleCustomerOrder extends AbstractModel
        implements SubmitAction { // (2)
        
	//.... Code Omitted ....
	
	@Override // (3)
    public boolean submit(long uid, List<Integer> ids, Map<String, Object> context) {
    	boolean result = false;
    	
    	//	Execute Parent (if any)
    	Optional<SubmitAction> optParent = this.getParent(SubmitAction.class); // (4)
        if ( optParent.isPresent() ) {
        	SubmitAction parent = optParent.get();
        	result = parent.submit(uid, ids, context);
        }
        
    	//.... Code Omitted ....
    	return result;
    }
    
	//.... Code Omitted ....
}
```

1. Import static ini digunakan untuk mempermudah penulisan `implements` Inner Interface-Class.
2. Definisi `implements` Inner Interface-Class yang diinginkan. Jika tidak ada tahap 1, maka penulisan implements akan menjadi `SaleCustomerOrderInterface.SubmitAction`.
3. Penulisan implementasi function yang ada di dalam Inner Interface-Class (Overriding).
4. Pemanggilan _parent_ jika terdapat inheritance di dalam model.

### Calls & Super

Pada bagian [Background](#background) telah dijelaskan pemanggilan function pada versi yang sebelumnya, berikut adalah cara pemanggilan function setelah adanya fitur Interfaced Model Definition.

```java
import static com.efitrac.module.sale.models.SaleCustomerOrderInterface.*; // (1)

@Model(name = "sale.sales_order")
public class SaleSalesOrder extends AbstractModel {
        
	//.... Code Omitted ....
	
	public void processImportantStuff(long uid, List<Integer> customerOrderIds, Map<String, Object> context) {
    	
       Optional<SubmitAction> optInterfacedModel = 
           PoolCache.getModel(SubmitAction.class); // (2)
        
        if ( optInterfacedModel.isPresent() ) {
            SubmitAction interfacedModel = optInterfacedModel.get();
            
           	interfacedModel.submit(uid, customerOrderIds, context); // (3)
        }
        
    	return result;
    }
    
	//.... Code Omitted ....
}
```

Snippet code di atas menampilkan tentang pemanggilan function yang ada pada Model Interface yang sudah ditulis pada bagian [Model Interface](#model_interface).

1. Import static digunakan untuk menyingkat penulisan Inner Interface-Class.
2. Mengambil Model Interface atas sebuah Inner Interface-Class.
3. Direct Calling function `#submit(uid, ids, context)`.

