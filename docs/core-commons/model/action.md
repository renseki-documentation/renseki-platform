# Action

Action adalah sebuah _public function_ yang dipunyai oleh sebuah Model dengan ciri-ciri signature sebagai berikut,

```java
public boolean coolFunInModel(long uid, List<Integer> ids, Map<String, Object> context) {
    boolean result = false;
        
    //	Do Something

    return result;
}
```

Perhatikan _parameter_ yang dipunyai oleh contoh function di atas, terdapat tiga parameter yang terdaftar dalam membuat function, 

1. uid : long
2. ids : List\<Integer>
3. context : Map\<String, Object>

Ciri pada definisi function tersebut yang menandakan sebuah function dapat dinamakan dengan **Action**. Sebuah Action dapat mengeluarkan sebuah _return-value_ yang bermacam-macam, tergantung kebutuhan (e.g. [Action Button](../../core-web/view/action_button#java_definition)).

## Requirement

| Dependency   | Version |
| ------------ | ------- |
| Core Commons | _Any_   |
| Core Web     | _Any_   |

## Action with Annotation

| Dependency   | Version                |
| ------------ | ---------------------- |
| Core Commons | _Any_                  |
| Core Web     | \>= 3.4012.04-SNAPSHOT |

Action with Annotation adalah sebuah fitur untuk menandai function yang berupa Action dengan sebuah annotation @Action. Annotation @Action berguna sebagai attribute tambahan untuk mempermudah _developer_ dalam mengimplementasikan sebuah Model.

### States Filtering

```java
import com.efitrac.commons.annotation.Action;

@Action(
	states = { "draft", "submitted" }
)
public boolean coolFunInModel(long uid, List<Integer> ids, Map<String, Object> context) {
    boolean result = false;
        
    //	Do Something

    return result;
}
```

!!! note
	States Filtering hanya dapat digunakan jika Model mempunyai field dengan nama **state** dengan tipe **selection**.

Fitur States Filtering akan membantu pengecekan daftar Record dengan ID yang dikirimkan pada parameter mempunyai nilai `state` sesuai dengan anotasi yang diberikan. Jika pada ID yang diberikan terdapat Record yang tidak mempunyai `state` yang sesuai, maka sistem akan mengeluarkan Error `InvalidRecordStatesForActionException`.