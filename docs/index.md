# Renseki Platform Developer Documentation

Dokumentasi ini berisikan tentang cara-cara penggunaan Renseki Platform, mulai dari **Core Commons** yang berisi tentang pembuatan *Model*, definisi *Field*, dll, sampai dengan pembuatan tampilan administrasi yang ada pada **Core Web**.

## How to Contribute

Untuk berkontribusi, berikut adalah poin-poin yang dapat dilakukan,

1. Mengontak [Administrator](mailto:ian.yulianto@renseki.com) untuk memberikan akses ke [Repository](https://gitlab.com/conec-tech/renseki/renseki-documentation).
2. Membuat [Merge Request](https://gitlab.com/conec-tech/renseki/renseki-documentation/merge_requests/new) dengan Title dan Description yang sesuai.
3. Jika Merge Request sudah di-approve oleh sejumlah pihak, maka akan dilanjutkan ke proses Merge dan siap untuk di-deploy ke internal server.