# Document Model Reference Button

Widget ini digunakan untuk memuat Dokumen Model lain sebagai referensi untuk pembuatan Record Model yang sekarang ini sedang dibuat. Event setelah pemilihan Dokumen Model disamakan dengan event **On Change** sebuah field. Dengan demikian, pemberian nilai pada form menjadi lebih mudah dan dinamis.

## Specification

| Key               | Value                         |
| ----------------- | ----------------------------- |
| Supported XML Tag | `<button />`                  |
| Widget Name       | `document_reference_selector` |

## Requirement

| Dependency   | Version                |
| ------------ | ---------------------- |
| Core Commons | \>= 3.4000.48-SNAPSHOT |
| Core Web     | \>= 3.3008.28-SNAPSHOT |

## Sneak Peak

![](img/model_document_reference_button/img_1.png)

![](img/model_document_reference_button/img_2.png)

![](img/model_document_reference_button/img_3.png)

## How To

### XML UI View

```xml
<button widget="document_reference_selector" onload="onLoadFn">
    <models>
        <!-- Basic Declaration -->
        <model name="res.contact_person" string="Contact Person" />        
        
        <!-- Model Label akan di-resolve dari #getDescription() atau #getModelName() -->
        <model name="res.contact_person" />
        
        <!-- Model dengan menggunakan DOMAIN -->
        <model name="customer.customer" string="Customer">        
            [('name', 'like', 'qwe')]
        </model>
    </models>
</button>
```

#### Variable Value Domain Support

Domain yang ada di dalam widget ini dapat dipakai bersamaan dengan variable nilai dari form. Adanya beberapa batasan dalam memakai *variable-value*, yaitu tidak bisa memakai variable-value dari field One-To-Many dan Many-To-Many. 

```xml
<!-- Button Widget Document Model Reference -->
<button widget="document_reference_selector" onload="onLoadContactPerson">
	<models>
		<model name="customer.customer" string="Customer">        
            [('payment_terms_id', '=', '$payment_terms_id'), ('name', 'like', '$name')]
        </model>
	</models>
</button>

<!-- Basic Definition -->
<horizontallayout height="10px" width="100%"></horizontallayout>
<horizontallayout width="100%" >
	<formlayout width="100%">
		<field name="name" />
		<field name="notes" width="75%" />
	</formlayout>
	<formlayout width="100%">
		<field name="payment_terms_id" />
	</formlayout>
</horizontallayout>
```

Pada contoh di atas, domain yang dipakai memakai variable-value **$payment_terms_id** dan **$name**.

!!! note "Limitation (tl;dr)"
	1. Field yang tidak bisa diproses One-To-Many & Many-To-Many
	2. Nilai *NULL* pada variable-value akan diabaikan dan tidak akan diproses (stay as is)

### Event `onload()` Declaration on Model

```java
//	Imports
import com.efitrac.commons.model.OnChangeReturnValue;
import console.vaadin.widget.component.button.ModelReference;
import console.vaadin.widget.component.button.OnLoadDocumentSelector;
import java.util.Map;

//	Function
public OnLoadDocumentSelector onLoadFn() {
    return new OnLoadDocumentSelector() {
        @Override
        public OnChangeReturnValue onLoad(
            long uid, ModelReference modelReference, Map<String, Object> context) {
            
            //	Prepare the result
            final OnChangeReturnValue res = new OnChangeReturnValue();
            
            //	Change value of field 'name' = "ASDOANSDOINASDN"
            res.addValue("name", "ASDOANSDOINASDN");
            
            //	Do the rest
            //	... Here ...

            return res;
        }
    };
}
```

