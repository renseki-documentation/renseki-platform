# Action button

Action Button adalah sebuah button yang berada di sebuah form yang mempunyai aksi memanggil function yang ada di dalam model tertentu. Action Button bisa menimbulkan beberapa _event_ sesuai dengan kebutuhan, berikut beberapa _event_ yang dapat ditimbulkan oleh Action Button:

1. Reload Page
2. Navigate to other View
3. Show a Report (Jasper)

## Specification

| Key               | Value      |
| ----------------- | ---------- |
| Supported XML Tag | `<button>` |

## Requirement

| Dependency   | Version |
| ------------ | ------- |
| Core Commons | _Any_   |
| Core Web     | _Any_   |

## Usage

### XML Definition

```xml
<!-- XML Definition for a View -->
<form string="Legendary View">
	<header>
		<actionbutton>
			<!-- (1) -->
			<button name="coolFunInModel" (2)
                    states="submitted, cancelled" (3)
                    string="Ez Button" (4)
                    size="small" (5)
                    type="red" (6)
                    groups="module.group_name,module.group_name" (7)
                    />
		</actionbutton>
	</header>
	<sheet>
	    <verticallayout>
	        <!-- (8) -->
	        <button name="someFuncInModel" string="Cool Button" />
	    </verticallayout>
	    <tabs>
	        <tab-page string="Some Awesome Tab Page">
	            <field name="field_x_to_many"> 
	                <!-- (9) -->
	                <tproperty name="funcInRelatedModelXToMany" widget="button" string="Super Awesome Mega Button" />
	            </field>
	        </tab-page>
	    </tabs>
	</sheet>
</form>
```

1. Definisi Action Button yang berada di bagian `<actionbutton />` (sebelum tampilan form). Action Button diawali dengan tag `<button>`.
2. Attribute `name` digunakan sebagai _identified_ dari sebuah function pada model yang akan dijalankan ketika Button ditekan (on-click event). Format penamaan disarankan untuk mengikuti _convention_ dari penamaan function di Java, yaitu **semiCamelCase**.
3. Attribute `states` mempunyai fungsi untuk menampilkan Button pada _state_ model tertentu. Attribute ini mempunyai ketergantungan dengan field dengan nama "state" pada model. Jika tidak ada field tersebut, maka attribute ini tidak mempunyai kegunaan apapun.
4. Sebuah Button perlu adanya label untuk ditunjukkan kepada _end-user_, attribute `string` mempunyai fungsi tersebut.
5. Ukuran Button dapat dibedakan menjadi dua macam, yaitu **small** dan **large**.
6. Dekorasi lain pada Button adalah berbentuk warna. Pada attribute `type` ini menyediakan tiga warna yang dapat dipilih, **red**, **blue**, **green**.
7. Definisi XML dapat menentukan Role apa yang boleh mengakses / melihat sebuah komponen, pada komponen Button. Untuk lebih lanjut, silahkan untuk membaca [Security Control - Model Access & Record Ruling](../../core-commons/security/security_controlling).
8. Selain dapat didefinisikan di dalam `<actionbutton />`, Action Button juga bisa didefinisikan di dalam layout.
9. Definisi terakhir ada pada bagian `<tproperty />` di mana tag tersebut dipakai di dalam menampilkan detil dari Field One To Many atau Many To Many.

### Java Definition

```java
@Model( name ="legendary.model_awesome" )
public class LegendaryModelAwesome extends AbstractModel {
    //... code omitted ...
    
    /**
    * Function yang akan di-binding ke XML View.
    *
    * @param uid User ID
    * @param ids List of Record IDs of Current Model
    * @param context Context
    *
    * @return Boolean
    */
    public boolean coolFunInModel(long uid, List<Integer> ids, Map<String, Object> context) {
        boolean result = false;
        
        //	Do Something
        
        return result;
    }
    
    //... code omitted ...
}
```

Parameter yang diperlukan untuk memakai Action Button pada bagian Java Definition adalah,

1. `uid` : Long
2. `ids` : List<Integer\>
3. `context` : Map<String, Object\>

## Button Post Event

Action button mempunyai _post event_ yang dibagi menjadi tiga tipe, yaitu

1. Reload Page
2. Navigate to other View
3. Show a Report (Jasper)

Definisi dari masing-masing tipe tersebut tergantung dari bagaimana definisi Java Definition function yang dipakai oleh Action Button. Berikut adalah penjelasan yang lebih detil dari masing-masing tipe Button Post Event.

### Reload Page

Sesuai dengan namanya, Button Post Event ini akan memuat ulang tampilan setelah melakukan proses yang dilakukan. 

```java
@Model( name ="legendary.model_awesome" )
public class LegendaryModelAwesome extends AbstractModel {
    //... code omitted ...
    
    /**
    * Function yang akan di-binding ke XML View.
    *
    * @param uid User ID
    * @param ids List of Record IDs of Current Model
    * @param context Context
    *
    * @return True: Reload || False: Nothing
    */
    public boolean coolFunInModel(long uid, List<Integer> ids, Map<String, Object> context) {
        boolean result = false;
        
        //	Do Something
        
        return result;
    }
    
    //... code omitted ...
}
```

### Navigate to other view

Button Post Event satu ini akan mengarahkan ke tampilan yang lain melalui definisi action (*ir.action.act_window*). 

```xml
<!--
= Example of XML Definition for a Action Reference for
= Button Post Event
--> 
 <record id="sale_sales_order_form_view" model="ir.ui.view">
    <field name="name">sale.sales_order.form</field>
    <field name="model">sale.sales_order</field>
    <field name="arch" type="xml">
        <!-- XMl View Definition -->
        <!-- ... code omitted ... -->
    </field>
</record>

<record id="sale_sales_order_form_action" model="ir.actions.act_window">
	<field name="name">Sales Order</field>
	<field name="type">ir.actions.act_window</field>
	<field name="res_model">sale.sales_order</field>
	<field name="view_mode">form</field>
	<field name="view_type">form</field>
	<field name="view_id" ref="sale_sales_order_form_view" />
</record>
```

```java
@Model( name ="legendary.model_awesome" )
public class LegendaryModelAwesome extends AbstractModel {
    //... code omitted ...
    
    /**
    * Function yang akan di-binding ke XML View.
    *
    * @param uid User ID
    * @param ids List of Record IDs of Current Model
    * @param context Context
    *
    * @return True: Reload || False: Nothing
    */
    public Object[] coolFunInModel(long uid, List<Integer> ids, Map<String, Object> context) {
        //	Do Something
        
        return new Object[]{
            "sale_sales_order_form_action", // (1)
            recordId, // (2)
            context // (3)
        };
    }
    
    //... code omitted ...
}
```

1. XML Record ID dari Action yang ingin ditargetkan. Tidak perlu menambahkan prefix `{module_name}.`.
2. (Nullable) Record ID dari model yang ditarget.
3. Context. Perhatian! Jangan memasukkan dengan nilai insialisasi baru (*new instance*) atau NULL.

### Navigate to other view (with default value)

| Dependency   | Version              |
| ------------ | -------------------- |
| Core Commons | _Any_                |
| Core Web     | >= 3.4000.1-SNAPSHOT |

Tidak hanya bisa mengarahkan ke tampilan lain, jika target tampilan berupa Form View dan `recordId` bernilai NULL, value dari field yang ditampilkan dapat diisikan value awal sesuai dengan kebutuhan. 

```java
@Model( name ="legendary.model_awesome" )
public class LegendaryModelAwesome extends AbstractModel {
    //... code omitted ...
    
    /**
    * Function yang akan di-binding ke XML View.
    *
    * @param uid User ID
    * @param ids List of Record IDs of Current Model
    * @param context Context
    *
    * @return Array of Object
    */
    public Object[] coolFunInModel(long uid, List<Integer> ids, Map<String, Object> context) {
        
        //	Add Default value for the target field in form view
        Map<String, Object> _context = new HashMap<>(context); // (1)
        _context.put("default_field_name_1", "some value"); // (2)
        _context.put("default_field_name_2", "some value");
        
        return new Object[]{
            "sale_sales_order_form_action", 
            null, // (3)
            _context // (4)
        };
    }
    
    //... code omitted ...
}
```

1. Disarankan untuk menginisialisasi context secara tersendiri jika ingin mengubah nilai dari context.
2. Pemakaian prefix `default_` diikuti dengan Field Name adalah cara berkomunikasi dengan target untuk memasukkan nilai awal.
3. Nilai NULL pada `recordId`.
4. Jangan lupa untuk memakai Context yang sudah diinisialisasi ulang.

### Show a Report (Jasper)

Tipe terakhir pada Button Post Event adalah menampilkan Report (Jasper) yang sudah didefinisikan. Untuk cara definisi Report (Jasper) bisa dilihat pada [Reporting With Jasper Report](../../core-commons/reporting/reporting_with_jasper_report).

```java
@Model( name ="legendary.model_awesome" )
public class LegendaryModelAwesome extends AbstractModel {
    //... code omitted ...
    
    /**
    * Function yang akan di-binding ke XML View.
    *
    * @param uid User ID
    * @param ids List of Record IDs of Current Model
    * @param context Context
    *
    * @return Action as Report Jasper
    */
    public Action coolFunInModel(long uid, List<Integer> ids, Map<String, Object> context) {
        Report report = PoolCache.<Report>getModel("report").get();

        //  Persiapkan parameter Jasper Report
        Map<String, Object> data = new HashMap<>();
        data.put("additional_data_parameter", "Some Value");
        
        //	Return as Report Action
        return report.getAction(ids, "report_name", data);
    }
    
    //... code omitted ...
}
```

## Confirmation Dialog

| Dependency   | Version               |
| ------------ | --------------------- |
| Core Commons | _Any_                 |
| Core Web     | \>= 3.4000.3-SNAPSHOT |

```xml
<button name="coolFunInModel"
        confirmation="true"
        />
```

