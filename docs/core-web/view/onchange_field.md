# Field On Change Event 

Field On Change Event adalah sebuah fitur yang akan menjalankan function pada sebuah model, di mana nilai kembalian dari function tersebut dapat mempengaruhi nilai dari field-field yang ada di sekelilingnya. Tidak semua tipe field dapat diberikan *event* **On Change**, berikut adalah daftar field yang mendukung,

1. Char
2. Text
3. Boolean
4. Integer
5. Float
6. Many To One

Field **Many To Many** belum mendapat dukungan dalam fitur Field On Change Event. Sedangkan Field **One To Many**, fitur ini dapat dipakai dalam property xml `<tproperty />`.

## Specification

| Key               | Value                         |
| ----------------- | ----------------------------- |
| Supported XML Tag | `<field />` & `<tproperty />` |
| Widget Name       | `field on change event`       |

## Requirement

| Dependency   | Version                |
| ------------ | ---------------------- |
| Core Commons | \>= 3.4010.00-SNAPSHOT |
| Core Web     | \>= 3.4000.0-SNAPSHOT  |

## Usage

```xml
<record id="sale_customer_order_form_view" model="ir.ui.view">
    <field name="name">sale.customer_order.form</field>
    <field name="model">sale.customer_order</field>
    <field name="arch" type="xml">
        <form string="Customer Order">
            <header>
                <actionbutton>
                    <button name="action_request_submit"
                            states="draft"
                            string="Submit"
                            size="small"
                            type="red"
                            groups="sale.group_supervisor,sale.group_manager"/>
                    <button name="action_request_auth"
                            states="draft"
                            string="Req. For Auth."
                            size="small"
                            type="blue"
                            groups="sale.group_supervisor,sale.group_manager"/>
                    <button name="action_request_submit_auth"
                            states="rq_auth"
                            string="Authorize"
                            size="small"
                            type="red"
                            groups="sale.group_manager"/>
                </actionbutton>
                <navigation>
                    <field name="state" widget="statusbar"/>
                </navigation>
            </header>
            <sheet>
                <verticallayout padding="true">
                    <horizontallayout width="100%" margin="false,true,true,false">
                        <formlayout width="100%">
                            <field name="name" width="60%" readonly="true"/>

                            <!-- Look at me! Field On Change Event -->
                            <field name="sales_id" string="Salesperson" width="60%"
                                   attrs="{'readonly': [('state', 'not in', ['draft'])]}"
                                   on_change="onChangeSales(sales_id, context)" groups="sale.group_supervisor"/>

                            <field name="customer_id" width="60%" attrs="{'readonly': [('state', 'not in', ['draft'])]}" on_change="onChangeSales(sales_id, context)"/>
                            <!-- -  -->

                            <field name="order_type" width="60%"/>
                        </formlayout>
                        <formlayout width="100%">
                            <field name="date" width="60%" readonly="true"/>
                            <field name="expected_delivery_date" width="60%"/>
                            <field name="expired_date" width="60%"/>
                        </formlayout>
                    </horizontallayout>
                    <tabs>
                        <tab-page string="Detail">
                            <field name="customer_order_line" attrs="{'readonly': [('state', 'not in', ['draft'])]}">
                                
                                <tproperty name="line_item" width="70px" />

                                <!-- Look at me! TProperty On Change Event -->
                                <tproperty name="productcategory_id" width="150px" on_change="onChangeCategory(productcategory_id, context)"/>
                                <tproperty name="product_id" width="200px" on_change="onProductChange(uid, parent.customer_id, product_id, uom_id, qty, disc_pct1, disc_pct2, disc_pct3, disc_pct4, disc_pct5, disc_value, context)" />
                                <tproperty name="uom_id" width="70px" on_change="onUomChange(uid, parent.customer_id, product_id, uom_id, qty, disc_pct1, disc_pct2, disc_pct3, disc_pct4, disc_pct5, disc_value, context)" />
                                <tproperty name="qty" width="70px" on_change="onUomChange(uid, parent.customer_id, product_id, uom_id, qty, disc_pct1, disc_pct2, disc_pct3, disc_pct4, disc_pct5, disc_value, context)" />
                                <!-- - -->

                                <tproperty name="price" width="150px" readonly="true" />
                                <tproperty name="gross_value" width="150px" readonly="true" />
                                <tproperty name="disc_pct1" width="70px" />
                                <tproperty name="disc_pct2" width="70px" />
                                <tproperty name="disc_pct3" width="70px" />
                                <tproperty name="disc_pct4" width="70px" />
                                <tproperty name="disc_pct5" width="70px" />
                                <tproperty name="disc_value" width="120px" />
                                <tproperty name="subtotal" width="150px" readonly="true"/>
                                <tproperty name="status_id" width="150px" />
                                <tproperty widget="button"
                                           name="order_line_split"
                                           string="Split"
                                           size="small"
                                           type="red" />
                            </field>
                            <horizontallayout width="100%">
                                <verticallayout align="middle-right" width="300px">
                                    <formlayout width="100%">
                                        <field name="gross_value" widget="monetary" options="IDR" readonly="true"/>
                                        <field name="total_price" widget="monetary" options="IDR" readonly="true"/>
                                    </formlayout>
                                </verticallayout>
                            </horizontallayout>
                        </tab-page>
                        <tab-page string="Notes">
                            <field name="notes" width="100%"  attrs="{'readonly': [('state', 'not in', ['draft'])]}"/>
                        </tab-page>
                    </tabs>
                </verticallayout>
            </sheet>
        </form>
    </field>
</record>
```

Cara pemakaian fitur Field On Change Event adalah dengan cara menambahkan *attribute* `onchange=fun_name(...)` pada xml tag `<field />` atau `<tproperty />`. Berikut adalah contoh pemakaiannya,

![](img/onchange_field_1.png)

![](img/onchange_java_func.png)

Terdapat dua *signature* parameter yang berbeda dalam fitur Field On Change Event, di mana salah satunya adalah signature yang sudah memasuki tahap *Legacy* atau *Deprecated*, sehingga disarankan untuk tidak memakai signature tersebut di kemudian hari.

### Legacy

```xml
<field name="sales_id" 
	   string="Salesperson" 
	   width="60%"                                   
   	   on_change="onChangeSales(sales_id, context)"/>
```

```java
//	Model Function for Legacy On Change
public OnChangeReturnValue onChangeSales(
		List<Integer> ids, Integer sales_id, Map<String, Object> context) {

    OnChangeReturnValue result = new OnChangeReturnValue();
    AbstractModel sales = PoolCache.getModel("sale.salesman").get().browseN(sales_id);

    Domain domain = new
            Domain.Builder("[('sales_id','='," + sales.getId() + ")]").build();

    result.addDomain("customer_id", domain);

    return result;
}
```

Masing-masing parameter akan merepresentasikan nilai dari field-field yang sudah didefinisikan pada tampilan. Selain itu, terdapat parameter global yang diberi kata kunci `uid` dan `context`.

### Form Values

```xml
<!-- With Form Value -->
<field name="customer_id" 
	   width="60%" 
	   on_change="onChangeSales(uid, this, context)"/>
```

```java
//	Model Function for Form Value On Change
public OnChangeReturnValue onChangeSales(long uid, FormValues values, Map<String, Object> context) {

    OnChangeReturnValue result = new OnChangeReturnValue();
  
  	Optional<Integer> optSalesId = values.get("sales_id");
  	if ( optSalesId.isPresent() ) {
        final int sales_id = optSalesId.get();
      
    	AbstractModel sales = PoolCache.getModel("sale.salesman").get().browseN(sales_id);

        Domain domain = new
                Domain.Builder("[('sales_id','='," + sales.getId() + ")]").build();

        result.addDomain("customer_id", domain);  
    }
    

    return result;
}
```

Peberdaan yang sangat terlihat pada dua Model Function ini adalah parameter yang harus didefinisikan. Pemakaian Form Values ini hanya perlu membuat function pada model yang menerima parameter `uid`, `formValues`, dan `context`. Nilai-nilai yang tampil pada tampilan di website akan ditampung pada tipe data `FormValues`.

#### Values Data Type

Di dalam Data Type `FormValues`, terdapat sebuah `Map` yang menampung nilai dari field-field yang ditampilkan, berikut adalah definisi tipe data yang digunakan sebagai presentasi masing-masing field,

| Field Type   | Data Type                 | Notes                                                     |
| ------------ | ------------------------- | --------------------------------------------------------- |
| Char         | String                    |                                                           |
| Text         | String                    |                                                           |
| Integer      | Integer                   |                                                           |
| Float        | BigDecimal                |                                                           |
| Boolean      | Boolean                   |                                                           |
| Date         | Date                      |                                                           |
| DateTime     | Date                      |                                                           |
| Many To One  | Integer                   | Record ID                                                 |
| Many To Many | List<Integer>             | Daftar Record ID                                          |
| One To Many  | List<Map<String, Object>> | Definisi yang ada di Map, sesuai dengan yang di tabel ini |

#### Parent

Di dalam `FormValues` terdapat function untuk mengambil nilai _parent_. Function ini dikhususkan untuk pemakaian `FormValues` di bagian `<tproperty />`. 

```java
//	Model Function for Form Value On Change
public OnChangeReturnValue onChangeSales(long uid, FormValues values, Map<String, Object> context) {

    OnChangeReturnValue result = new OnChangeReturnValue();
  
  	//...    Code Omitted ...

    Optional<FormValues> optParent = values.getParent();
    if ( optParent.isPresent() ) {
        final FormValues parentValues = optParent.get();
        
        //..    Do Something
    }

    //...    Code Omitted ...
    

    return result;
}
```

