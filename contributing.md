# How To Contribute

Proses kontribusi bisa dilakukan dengan langkah-langkah sebagai berikut,

1. Buat _branch_ baru
2. Tuliskan tambahan atau perubahan yang Anda inginkan
3. Buat [_Merge Request_](#merge_request) ke terhadap branch `master`
4. Berikanlah notifikasi kepada [_Maintainer_](#maintainer) jika terdapat Merge Request yang Anda buat
5. Jika Merge Request tersebut telah disetujui oleh Maintainer, maka dalam 2x24 jam, tulisan Anda akan ditampilkan pada dokumentasi resmi Renseki Platform

## What To Write

Ketika berhadapan dengan dokumentasi atau dengan hal penulisan sebuah dokumen, masing-masing orang mempunyai _flavor_ atau ciri khas masing-masing yang tidak dapat dianggap sebelah mata. Maka dari itu, penulisan dokumentasi ini tidak dibatasi harus menggunakan format seperti apa, atau harus dengan bahasa seperti apa, namun terdapat beberapa poin yang harus ada dalam penulisan dokumentasi.

1. Contoh pemakaian berupa potongan `code` atau `xml` 
2. Hasil pemakaian fitur berupa gambar jika ada
3. ??

## Merge Request

Selain hanya memberikan perubahan, silahkan mengisikan deskripsi pada Merge Request yang berisikan tentang bagaimana cara Anda memakai fitur tersebut, atau bagaimana Anda mengetahui jika ada kesalahan pada dokumentasi. Merge Request akan dianggap _approved_ jika terdapat lebih dari **60% vote** _thumb-up_ dari Maintainer.

## Maintainer

Berikut adalah daftar maintainer dari Renseki Platform Documentation,

1. Michael Ronny ([michael.ronny@renseki.com](mailto:michael.ronny@renseki.com))
2. Ian Yulianto ([ian.yulianto@renseki.com](mailto:ian.yulianto@renseki.com))
3. Erick Pranata ([erick.pranata@renseki.com](mailto:erick.pranata@renseki.com))

