# Renseki Platform Documentation

Repository ini akan berisi tentang dokumentasi dari Renseki Platform. Di dalam dokumentasi ini akan terbagi menjadi dua bagian, yaitu **Commons** dan **Web**. Dokumentasi ini tidak membahas tentang hal lain selain Renseki Platform. Dimohon kerjasamanya untuk berkontribusi dalam melengkapi dokumentasi fungsionalitas dari Renseki Platform. Untuk berkontrobusi, silahkan untuk membaca bagian [How To Contribute](/contributing.md).

| Key     | Description                                         |
| ------- | --------------------------------------------------- |
| Commons | _Model_, _field_, _business-process_, etc           |
| Web     | Web XML, Form View, Tree View, On Change, Menu, etc |

Jika ada pertanyaan tentang Renseki Platform yang terdata di dalam dokumentasi ini, silahkan untuk mencari developer Renseki di sisi kiri atau kanan Anda, dan bertanyalah apa yang ingin tanyakan. Setelah mengerti langkah-langkah yang dibutuhkan, jangan lupa untuk berkontribusi menuliskan informasi tersebut dalam dokumentasi ini. Kontribusi Anda sangat berharga untuk kemajuan Renseki Platform.

## Running Documentation  (Unix)

1. Install `pip`
2. `$ pip install --user mkdocs`
3. `$ pip install --user mkdocs-material`
4. `$ mkdocs serve`

## Running Documentation (Windows)

*No one use Windows for hardcore development. Meh!*

 